import React, { Component } from 'react';
import { ActivityIndicator, AsyncStorage } from 'react-native'

import {
  StyleSheet,
  View,
} from 'react-native';

class AuthLoader extends Component {
	
	constructor(props) {
	    super(props);	    
	    this._bootstrapAsync();
	}

	_bootstrapAsync = async () => {
		const userToken = await AsyncStorage.getItem('userToken')
		this.props.navigation.navigate(userToken ? 'App' : 'Auth')
	}

	render() {
		return (
			 <View style={{ flex:1, backgroundColor:'white',justifyContent:'center',alignItems:'center' }}>
			 	<ActivityIndicator />
			 </View>
		);
	}
}

export default AuthLoader;