import React, { Component } from 'react';

import {
  StyleSheet,
  View,
  Text
} from 'react-native';

import { withNavigation } from 'react-navigation'
import { Container, Content, Header, Left, Right, Button, Icon } from 'native-base'

const AppHeader = ({ navigation }) => (

	<Header style={ styles.header }>
  		<Left style={ styles.left }>
  			<Button transparent onPress={()=> navigation.toggleDrawer() }>
  				<Icon name="md-menu" style = { styles.iconMenu } />
  			</Button>
  			<View style={{ paddingTop:12 }}>
  				<Text style={ styles.headerTitle }>Point of Sales</Text>
  			</View>
  		</Left>
  		<Right>
  			<Button transparent onPress={ () => navigation.navigate('Auth') }>
      			<Icon name="ios-log-out" style={ styles.iconRight } />
  			</Button>
  		</Right>
  	</Header>  	
)

const styles = StyleSheet.create({
	header:{
		height: 50,
		backgroundColor:'#0EB4CB',
		borderBottomColor: '#CC0000' 
	},
	headerTitle:{
		fontSize: 14, 
		fontWeight:'bold', 
		color: 'white',
		justifyContent:'center' 
	},
	left:{
		flexDirection:'row'
	},
	iconMenu:{
		fontSize: 22, color: 'white'
	},
	iconRight:{
		fontSize: 22, color: 'white',marginRight:4
	}
});

export default withNavigation(AppHeader);