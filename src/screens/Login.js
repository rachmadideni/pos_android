import React, { Component } from 'react';

import {
  StyleSheet,
  View,
  Image,
  ImageBackground,
  TouchableOpacity,
  AsyncStorage
} from 'react-native';

import {
	Container,
	Content,
	Form,
	Item,
	Input,
	Label,
	Button,
  Text,
	Icon
} from 'native-base'

class Login extends Component {
  constructor(props) {
    super(props);  
    this.state = {
      username:'',
      password:''
    };    
  }

  _signInAsync = async () => {
    await AsyncStorage.setItem('userToken','User')
    this.props.navigation.navigate('App')
  }

  render() {
    return (
      <Container>
      	<Content>
      		<Form>
      			<Item floatingLabel>
      				<Icon name="ios-person" />
      				<Label>username</Label>
      				<Input returnKeyType={"done"} name="username" onChangeText={(text)=> this.setState({ username:text})} />
      			</Item>
            <Item floatingLabel>
              <Icon name="ios-lock" />
              <Label>password</Label>
              <Input secureTextEntry={true} returnKeyType={"done"} name="password" onChangeText={(text)=> this.setState({ password:text})} />
            </Item>
            <Button onPress={ this._signInAsync } success block style={{ marginTop:20,borderRadius:4 }}>
              <Text>LOGIN</Text>
            </Button>

            <View style={{ 
                flexDirection:'row',
                alignItems:'center',
                marginHorizontal:20,
                marginTop:15 }} >

              <View style={{ 
                  flex:1,
                  alignItems:'center' }}>

                <TouchableOpacity onPress={()=> this.props.navigation.navigate('Daftar')}>
                  <Text style={{ fontSize:14,color:'grey' }}>Buat Akun baru</Text>
                </TouchableOpacity>
              </View>
              <View style={{ flex:1,alignItems:'center' }}>
                <TouchableOpacity onPress={()=> this.props.navigation.navigate('LupaPass')}>
                  <Text style={{ fontSize:14,color:'grey' }}>Lupa Password ?</Text>
                </TouchableOpacity>
              </View>
            </View>

      		</Form>
      	</Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({

});


export default Login;