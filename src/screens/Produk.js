import React, { Component } from 'react';

import {
  StyleSheet,
  View,
  TouchableOpacity
} from 'react-native';

import { Container,Content,Header,Body,Text,Form,Item,Label,Input,Button,Icon,Picker } from 'native-base'

// import AppHeader from './AppHeader';

class Produk extends Component {

    /*static navigationOptions = ({ navigation }) => {
      
        const { params = {} } = navigation.state
        
        let headerStyle =  { backgroundColor: 'red' }
        let headerTitle = 'Point of Sales'
        let headerTitleStyle = { color:'#666666',marginLeft:50 }

        let headerLeft = (
            <View style={{ marginVertical:5,marginLeft:5 }}>
                <TouchableOpacity style={{ padding:5,borderRadius:10 }} onPress={ navigation.getParam('onDrawerClick') }>
                    <Icon name="md-menu" style={{ marginHorizontal:10 }} />                    
                </TouchableOpacity>
            </View>
        )

        let headerRight = (
            <View style={{ marginVertical:5,marginRight:5 }}>
                <TouchableOpacity style={{ 
                    flexDirection:'row',
                    alignItems:'center',
                    justifyContent:'center',
                    padding:5,
                    borderRadius:10 
                }} onPress={ navigation.getParam('onLogout') }>
                    <Icon name="ios-log-out" style={{ marginRight:10 }}/>
                    <Text style={{ color:'black' }}>LOGOUT</Text>
                </TouchableOpacity>
            </View>
        )       

        return { headerStyle, headerTitle, headerTitleStyle, headerLeft, headerRight }
    }*/

  render() {
    return (
      
      <Container>
          {/*<AppHeader navigation={this.props.navigation} />*/}
          <Content style={{ flex:1,backgroundColor:'white',paddingHorizontal:15 }}>                                
                  <Form>
                      <Item floatingLabel>
                          <Label>Nama Produk</Label>
                          <Input returnKeyType={"done"} />
                      </Item>
                      <Item floatingLabel>
                          <Label>Deskripsi</Label>
                          <Input returnKeyType={"done"} />
                      </Item>
                      <Item floatingLabel>
                          <Label>Custom Tag</Label>
                          <Input returnKeyType={"done"} />                          
                      </Item>
                      <Item floatingLabel>
                          <Label>Harga 1</Label>
                          <Input returnKeyType={"done"} />                          
                      </Item>
                      <Item floatingLabel>
                          <Picker mode="dropdown" 
                            iosIcon={<Icon name="ios-arrow-down-outline" />}
                            style={{ width: undefined }}
                            placeholder="Select your SIM"
                            placeholderStyle={{ color: "#bfc6ea" }}
                            placeholderIconColor="#007aff">
                            <Picker.Item label="Wallet" value="key0" />
                            <Picker.Item label="ATM Card" value="key1" />
                            <Picker.Item label="Debit Card" value="key2" />
                            <Picker.Item label="Credit Card" value="key3" />
                            <Picker.Item label="Net Banking" value="key4" />
                          </Picker>                          
                      </Item>
                      <Item floatingLabel>
                          <Label>Harga</Label>
                          <Input returnKeyType={"done"} />                          
                      </Item>
                      <Button primary block style={{ marginTop:20,marginHorizontal:20,borderRadius:4 }}>
                          <Text>Simpan</Text>
                      </Button>
                      <Button warning block style={{ marginTop:10,marginHorizontal:20,borderRadius:4 }}>
                          <Text>Batal</Text>
                      </Button>
                  </Form>                                
          </Content> 
            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                		
            </View>
      </Container>

    );
  }
}

const styles = StyleSheet.create({

});


export default Produk;