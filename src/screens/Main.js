import React, { Component } from 'react';

import {
  StyleSheet,
  View,
} from 'react-native';

import { Container, Content, Header, Body, Button, Text } from 'native-base'
import { DrawerNavigator, DrawerItems } from 'react-navigation'

// screen
import Produk from './Produk'
import Inventori from './Inventori'
import Transaksi from './Transaksi'
import Retur from './Retur'
import Setting from './Setting'


const customDrawerContent = (props) => (

	<Container>
        <Header style = { styles.drawerHeader }>
            <Body style={ styles.drawerBody}>
                {/*<Image style = { styles.drawerImage } source={require('../../assets/wallpaper.png')} />*/}
            </Body>
            <View style = { styles.drawerBodyContent}>               
                <Text style={{ fontSize:13,fontWeight:'bold',color:'#999999' }}>NAMA AKUN</Text>                
                <Text style={{ fontSize:10,fontWeight:'bold',color:'#666',margin:0,padding:0 }}>LOREM</Text>
                <Text style={{ fontSize:11,padding:0,margin:0}}>lorem ipsum</Text>
                <Button block success style = { styles.drawerHeaderButton }>
                    <Text style = { styles.drawerHeaderButtonText }>PUSH ME</Text>
                </Button>
            </View>
        </Header>
        <Content>
            <DrawerItems {...props} />            
        </Content>
    </Container>

)

const AppDrawerNavigator = new DrawerNavigator({
	Produk:Produk,
	Inventori:Inventori,
	Transaksi:Transaksi,
	Retur:Retur,
	Setting:Setting
},{
	initialRouteName: 'Produk',
    contentComponent: customDrawerContent,
    drawerOpenRoute: 'DrawerOpen',
    drawerCloseRoute: 'DrawerClose',
    drawerToggleRoute: 'DrawerToggle',
    drawerPosition:'left',
    drawerWidth:260
})

const styles = StyleSheet.create({
	drawerHeader:{
		backgroundColor:'white', height:120
	},
	drawerHeaderButton:{
		height:28,
        paddingHorizontal:5,
        paddingVertical:10,
        borderRadius:2,
        marginHorizontal:0,
        marginVertical:10
	},
	drawerHeaderButtonText:{
		fontSize:10,
        fontWeight:'bold',
        color:'white',
        marginVertical:5
	},
	drawerBody:{
		backgroundColor:'transparent',width:90
	},
	drawerBodyContent:{
		flex: 1,
	    marginLeft: 0,
	    marginTop: 15,
	    backgroundColor: 'transparent'
	},
	drawerImage:{
        width: 75,
	    height: 75,
	    borderRadius: 100,
	    marginLeft: 15
    }
});


class Main extends Component {
  render() {
    return (
      <AppDrawerNavigator />
    );
  }
}




export default Main;