import { createStackNavigator } from 'react-navigation'

import Produk from './Produk'
import Inventori from './Inventori'
import Retur from './Retur'
import Transaksi from './Transaksi'

// stack utk yg sdh otentikasi
const ScreenStack = createStackNavigator(
	{ 
		Produk:Produk,
		Inventori:Inventori,
		Retur:Retur,
		Transaksi:Transaksi
	}
);

export default ScreenStack;