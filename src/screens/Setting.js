import React, { Component } from 'react';
import { StyleSheet,View } from 'react-native';

import {
	Container,Content,Form,Item,
	Input,Label,Button,Text,Icon
} from 'native-base'

import AppHeader from './AppHeader';

class Setting extends Component {
  render() {
    return (
       <Container style={{ backgroundColor:'white' }}>
      	<AppHeader navigation={this.props.navigation} /> 
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
          <Text>My Setting Screen</Text>            		
      	</View>
      </Container>
    );
  }
}

const styles = StyleSheet.create({

});

export default Setting;