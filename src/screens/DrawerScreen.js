import React, { Component } from 'react';
import { NavigationActions, DrawerNavigator, DrawerActions, DrawerItems } from 'react-navigation'

import {
  StyleSheet,
  ScrollView,
  View,
  Text
} from 'react-native';

// drawer stack
import Setting from '../screens/Setting'

// application stack
import Home from '../screens/Home'

const AppDrawerNavigator = new DrawerNavigator(
  {
    Home:{
      screen:Home
    },
    Setting:{
      screen:Setting
    }
  },{
    initialRouteName:'Home',
    contentComponent:DrawerContent,
    drawerWidth:300,
    drawerPosition:'left'
  }
);

class DrawerContent extends Component {
  render(){
    return (
      <View>
        <ScrollView>
          <DrawerItems {...props} />
        </ScrollView>
      </View>
        
    );
  }
}

class DrawerScreen extends Component {

	navigateToScreen = (route) => () => {
		const navigateAction = NavigationActions.navigate({
			routeName:route
		})

		this.props.navigation.dispatch(navigateAction)
		this.props.navigation.dispatch(DrawerActions.closeDrawer())

	}

  render() {
    return (
      <AppDrawerNavigator />
    );
  }
}


export default DrawerScreen;