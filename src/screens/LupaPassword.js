import React, { Component } from 'react';

import {
  StyleSheet,
  View,
  Image,
  ImageBackground,
  TouchableOpacity
} from 'react-native';

import {
	Container,
	Content,
	Form,
	Item,
	Input,
	Label,
	Button,
  Text,
	Icon
} from 'native-base'

class LupaPassword extends Component {
  constructor(props) {
    super(props);  
    this.state = {
      email:'',
      password_baru:''
    };    
  }  

  render() {
    return (
      <Container>
      	<Content>
      		<Form>
      			<Item floatingLabel>
      				<Icon name="ios-mail" />
      				<Label>masukkan email yang valid</Label>
      				<Input returnKeyType={"done"} onChangeText={(text)=> this.setState({ email:text})} />
      			</Item>
            <Item floatingLabel>
              <Icon name="ios-lock" />
              <Label>password Baru anda</Label>
              <Input secureTextEntry={true} returnKeyType={"done"} name="password" onChangeText={(text)=> this.setState({ password_baru:text})} />
            </Item>
            <Button success block style={{ marginTop:20,borderRadius:4 }}>
              <Text>KONFIRMASI</Text>
            </Button>

            <View style={{ 
                flexDirection:'row',
                alignItems:'center',
                marginHorizontal:20,
                marginTop:15 }} >

              <View style={{ 
                  flex:1,
                  alignItems:'center' }}>

                <TouchableOpacity onPress={()=> this.props.navigation.navigate('Daftar')}>
                  <Text style={{ fontSize:14,color:'grey' }}>Buat Akun baru</Text>
                </TouchableOpacity>
              </View>
              <View style={{ flex:1,alignItems:'center' }}>
                <TouchableOpacity>
                  <Text style={{ fontSize:14,color:'grey' }}>Lupa Password ?</Text>
                </TouchableOpacity>
              </View>
            </View>

      		</Form>
      	</Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({

});


export default LupaPassword;