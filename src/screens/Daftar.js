import React, { Component } from 'react';

import {
  StyleSheet,
  View,
  Image,
  ImageBackground,
  TouchableOpacity
} from 'react-native';

import {
	Container,
	Content,
	Form,
	Item,
	Input,
	Label,
	Button,
  Text,
	Icon
} from 'native-base'

class Daftar extends Component {
  constructor(props) {
    super(props);  
    this.state = {
      email:'',
      fullname:'',
      notelp:'',
      password:'',
      password_konfirmasi:''
    };    
  }  

  render() {
    return (
      <Container>
      	<Content>
      		<Form>
      			<Item floatingLabel>
      				<Icon style={{color: '#CCCCCC' }} name="ios-card" />
      				<Label>Nama Lengkap</Label>
      				<Input returnKeyType={"done"} onChangeText={(text)=> this.setState({ fullname:text})} />
      			</Item>
      			<Item floatingLabel>
      				<Icon style={{color: '#CCCCCC' }} name="ios-mail" />
      				<Label>email</Label>
      				<Input returnKeyType={"done"} onChangeText={(text)=> this.setState({ email:text})} />
      			</Item>
	            <Item floatingLabel>
		            <Icon style={{color: '#CCCCCC' }} name="ios-lock" />
		            <Label>password</Label>
		            <Input secureTextEntry={true} returnKeyType={"done"} onChangeText={(text)=> this.setState({ password:text})} />
	            </Item>
	            <Item floatingLabel>
		            <Icon style={{color: '#CCCCCC' }} name="ios-lock" />
		            <Label>konfirmasi password</Label>
		            <Input secureTextEntry={true} returnKeyType={"done"} onChangeText={(text)=> this.setState({ password_konfirmasi:text})} />
	            </Item>
            <Button success block style={{ marginTop:20,borderRadius:4 }}>
              <Text>Daftar</Text>
            </Button>

            <View style={{ 
                flexDirection:'row',
                alignItems:'center',
                marginHorizontal:20,
                marginTop:15 }} >
              <View style={{ flex:1,alignItems:'center' }}>
                <TouchableOpacity onPress={()=> this.props.navigation.navigate('Login')}>
                  <Text style={{ fontSize:14,color:'grey' }}>Sudah punya Akun ?</Text>
                </TouchableOpacity>
              </View>               
            </View>

      		</Form>
      	</Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({

});


export default Daftar;