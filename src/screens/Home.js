import React, { Component } from 'react';

import {
  StyleSheet,
  View,
  Image,
  ImageBackground,
  TouchableOpacity,
  AsyncStorage,
  ActivityIndicator
} from 'react-native';

import {
	Container,
	Content,
    Header,
	Form,
	Item,
	Input,
	Label,
	Button,
    Text,
	Icon
} from 'native-base'

import { createBottomTabNavigator, DrawerNavigator, DrawerItems } from 'react-navigation'

import Produk from './Produk'
import Setting from './Setting'


const DrawerContentComponent = (props) => (
    <Container>        
        <Content>
            <DrawerItems {...props} />            
        </Content>
    </Container>
)

const AppDrawerNavigator = new DrawerNavigator(
    {        
        Produk:Produk,
        Setting:{ screen: Setting }
    },{
        initialRouteName:'Produk',
        contentComponent: DrawerContentComponent,
        drawerOpenRoute: 'DrawerOpen',
        drawerCloseRoute: 'DrawerClose',
        drawerToggleRoute: 'DrawerToggle',
        drawerPosition:'left'
    })


class Home extends Component {

  constructor(props) {
    super(props);    
  }

  /*static navigationOptions = ({ navigation }) => {
    drawerLabel:'Home',
    drawerIcon: ({ tintColor }) => (
      <Icon name="ios-menu" />
    ),
    headerStyle: { backgroundColor: '#FAFAFA' }
    
    // return { headerStyle }
  }*/
  
  static navigationOptions = ({ navigation }) => {
      
        const { params = {} } = navigation.state
        
        let headerStyle =  { backgroundColor: '#FAFAFA' }
        let headerTitle = 'Point of Sales'
        let headerTitleStyle = { color:'#666666',marginLeft:50 }

        let headerLeft = (
            <View style={{ marginVertical:5,marginLeft:5 }}>
                <TouchableOpacity style={{ padding:5,borderRadius:10 }} onPress={ navigation.getParam('onDrawerClick') }>
                    <Icon name="md-menu" style={{ marginHorizontal:10 }} />                    
                </TouchableOpacity>
            </View>
        )

        let headerRight = (
            <View style={{ marginVertical:5,marginRight:5 }}>
                <TouchableOpacity style={{ 
                    flexDirection:'row',
                    alignItems:'center',
                    justifyContent:'center',
                    padding:5,
                    borderRadius:10 
                }} onPress={ navigation.getParam('onLogout') }>
                    <Icon name="ios-log-out" style={{ marginRight:10 }}/>
                    <Text style={{ color:'black' }}>LOGOUT</Text>
                </TouchableOpacity>
            </View>
        )       

        return { headerStyle, headerTitle, headerTitleStyle, headerLeft, headerRight }
  }

  componentDidMount(){
    this.props.navigation.setParams({ onLogout: this._logOutAsync, isSaving:false })
    this.props.navigation.setParams({ onDrawerClick: this._DrawerOpen })
  }

  _onSave(){
        alert('you pressed save')
        this.props.navigation.setParams({ isSaving:true })

        setInterval(()=> {
            this.props.navigation.setParams({ isSaving:false })
        },300)
  }

  _logOutAsync = async () => {
      await AsyncStorage.clear();
      this.props.navigation.navigate('Auth');
  };

  _DrawerOpen = () => {
    // alert('drawer click')
    this.props.navigation.openDrawer()
  } 

  render() {
    return (
      <AppDrawerNavigator />
    );
  }
}

/*
<Container style={{ flex:1,alignItems:'center',justifyContent:'center' }}>
<Content contentContainerStyle={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
<Text>Home Screen</Text>                  
</Content>
</Container>
*/



/*export default createBottomTabNavigator({
    Home:{
        screen:Home,
        navigationOptions:{
            tabBarLabel:'Home',
            tabBarIcon:({tintColor})=>(
                <Icon name="ios-home" style={{ fontSize:16 }} />
            )       
        }        
    },
    Setting:{
        screen:Setting
    }
},{ headerMode:'screen'})*/

export default Home;