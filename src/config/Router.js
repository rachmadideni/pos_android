import React, { Component } from 'react';
import {View,Text,StyleSheet,Platform,TouchableOpacity,Image,StatusBar} from 'react-native';
import { Icon } from 'native-base'
import { createStackNavigator, createSwitchNavigator, createDrawerNavigator, DrawerActions } from 'react-navigation'
// import DrawerScreen from '../screens/DrawerScreen'

// auth loader
import AuthLoader from '../screens/AuthLoader'

// auth stack
import Login from '../screens/Login'
import Daftar from '../screens/Daftar'
import LupaPassword from '../screens/LupaPassword'

// drawer stack
// import Main from '../screens/Main'
import Setting from '../screens/Setting'

// application stack
import Home from '../screens/Home'


// import ScreenStack from '../screens/ScreenStack'

import Produk from '../screens/Produk'
import Inventori from '../screens/Inventori'
import Retur from '../screens/Retur'
import Transaksi from '../screens/Transaksi'

// stack utk yg belum otentikasi
const AuthStack = createStackNavigator(
{
    Login: { screen: Login },
    Daftar: { screen: Daftar },
    LupaPass: { screen: LupaPassword }
});


const DrawerNavigator = createDrawerNavigator({
	Produk:Produk,
	Inventori:Inventori,
	Retur:Retur,
	Transaksi:Transaksi
},{
	initialRouteName:'Produk'
})


const MenuImage = ({navigation}) => {
    if(!navigation.state.isDrawerOpen){
        return <Icon name="ios-menu" style={{ color:'white',marginLeft:20 }}/>
    }else{
        return <Icon name="ios-arrow-dropleft" style={{ color:'white',marginLeft:20 }}/>
    }
}


const AppStack = createStackNavigator({
	DrawerNavigator: DrawerNavigator
},{
	navigationOptions: ({ navigation }) => ({
		title:'POS',
		headerLeft:
		<TouchableOpacity  onPress={() => {navigation.dispatch(DrawerActions.toggleDrawer())} }>
            <MenuImage style="styles.bar" navigation={navigation}/>
        </TouchableOpacity>,
		headerStyle:{ backgroundColor:'#0EB4CB',borderBottomColor:'#CC0000' },
		headerTitleStyle:{
			color:'white',
			fontWeight:'bold'
		}
	})
})

// Main:Main


const Router = createSwitchNavigator({	
		AuthLoader:AuthLoader,
		App:AppStack,
		Auth:AuthStack
	},{
		initialRouteName: 'AuthLoader'
	});

export default Router