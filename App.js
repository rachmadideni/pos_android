import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View
} from 'react-native';

import Router from './src/config/Router'

export default class App extends Component<Props> {
  render() {
    return (
      <Router />
    );
  }
}
